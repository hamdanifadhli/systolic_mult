def dff_generator():
    file = open("./flipflop/dff.v","w+")
    file.write("""module dff
    #(parameter N= 3'd7)
    (
      input wire clk,
      input wire rst_n,
      input wire en,
      input wire [N:0] d,
      output wire [N:0] q
    );
    
    reg [N:0] data;
    
    always @(posedge clk)
    begin
      if (!rst_n)
        begin
          data <= 0;
        end
      else if (en)
        begin
          data <= d;
        end
      else
        begin
          data <= data;
        end
    end
    
    assign q = data;
  
endmodule
""")