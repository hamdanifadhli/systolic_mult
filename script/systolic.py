def sys_generator():
    panjang = input("masukkan panjang array: ")
    besar = input("masukkan besar bit integer input: ")
    file = open("./systolic/systolic.v","w+")
    header = """module SystolicArray(
    input clk,
    input rst_n,
    input en,\n"""

    footer = """);\t// module systolic array"""

    file.write(header)

    for i in range(int(panjang)):
        file.write(f'\tinput [{int(besar)-1}:0] A{i},\n')

    for i in range(int(panjang)):
        file.write(f'\tinput [{int(besar)-1}:0] B{i},\n')

    for i in range((int(panjang)-1)*2+1):
        if(i == ((int(panjang)-1)*2+1)-1):
            file.write(f'\toutput [{int(besar)*2-1}:0] out{i}\n')
        else:
            file.write(f'\toutput [{int(besar)*2-1}:0] out{i},\n')
            

    file.write(footer)

    file.write(f"""

\twire [{int(besar)*2-1}:0] zero = 0;
""")

    wait = ""
    for i in range((int(panjang)-1)*(int(panjang)-1)):
        wait = wait + f"wait{i}, "
    wait = wait[0:len(wait)-2] + ";"
    file.write(f"\twire [{int(besar)*2-1}:0] {wait}\n\n")

    count = []
    count_sum = []
    count_wait = []

    for i in range((int(panjang)-1)*(int(panjang)-1),0,-1):
        count_sum.append(i-1)
        count_wait.append(i-1)

    for i in range((int(panjang)-1)*2+1,0,-1):
        count.append(i-1)

    for i in range(int(panjang)):
        for j in range(int(panjang)):
            if(i == 0):
                if(j == 0):
                    file.write(f"""
    PE #(.B({int(besar)-1})) ur{j}c{i}(
        .clk(clk),
        .rst_n(rst_n),
        .en(en),
        .d1(A{j}),
        .d2(B{i}),
        .out(out{count.pop()}),
        .sum(zero)
    );
    """)
                else:
                    file.write(f"""
    PE #(.B({int(besar)-1})) ur{j}c{i}(
        .clk(clk),
        .rst_n(rst_n),
        .en(en),
        .d1(A{j}),
        .d2(B{i}),
        .out(wait{count_wait.pop()}),
        .sum(zero)
    );
    """)
            elif(i==int(panjang)-1 and j==int(panjang)-1):
                file.write(f"""
    PE #(.B({int(besar)-1})) ur{j}c{i}(
        .clk(clk),
        .rst_n(rst_n),
        .en(en),
        .d1(A{j}),
        .d2(B{i}),
        .out(out{count.pop()}),
        .sum(zero)
    );
    """)
            elif(j == 0):
                file.write(f"""
    PE #(.B({int(besar)-1})) ur{j}c{i}(
        .clk(clk),
        .rst_n(rst_n),
        .en(en),
        .d1(A{j}),
        .d2(B{i}),
        .out(out{count.pop()}),
        .sum(wait{count_sum.pop()})
    );
    """)
            elif(j == int(panjang)-1):
                file.write(f"""
    PE #(.B({int(besar)-1})) ur{j}c{i}(
        .clk(clk),
        .rst_n(rst_n),
        .en(en),
        .d1(A{j}),
        .d2(B{i}),
        .out(wait{count_wait.pop()}),
        .sum(zero)
    );
    """)
            elif(i == int(panjang)-1):
                file.write(f"""
    PE #(.B({int(besar)-1})) ur{j}c{i}(
        .clk(clk),
        .rst_n(rst_n),
        .en(en),
        .d1(A{j}),
        .d2(B{i}),
        .out(out{count.pop()}),
        .sum(wait{count_sum.pop()})
    );
    """)
            else:
                file.write(f"""
    PE #(.B({int(besar)-1})) ur{j}c{i}(
        .clk(clk),
        .rst_n(rst_n),
        .en(en),
        .d1(A{j}),
        .d2(B{i}),
        .out(wait{count_wait.pop()}),
        .sum(wait{count_sum.pop()})
    );
    """)

    file.write("\nendmodule")

    file.close()

    file = open("testbench.txt","w+")
    file.write("""
force -freeze sim:/SystolicArray/en 1 0, 0 {4 ns} -r 8
force -freeze sim:/SystolicArray/clk 1 0, 0 {5 ns} -r 10
""")

    for i in range(int(panjang)):
        file.write(f"force -freeze sim:/SystolicArray/A{i} 8'd{i} 0\n")

    for i in range(int(panjang)):
        file.write(f"force -freeze sim:/SystolicArray/B{i} 8'd{i} 0\n")

    file.close()

def pe_generator():
    file = open("./systolic/pe.v","w+")
    file.write("""module PE #(parameter B =3'd7) (clk, rst_n, en, d1, d2, out, sum);
    input clk, rst_n, en;
    input [B:0] d1,d2;
    input [2*B+1:0] sum;
    
    output [2*B+1:0] out;
    
    wire [2*B+1:0] w, wOut, wSum;
    
    assign w = (d1*d2) + wSum;
    assign out = wOut;
    
    dff #(.N(2*B+1)) D1(
      .clk(clk),
      .rst_n(rst_n),
      .en(en),
      .d(w),
      .q(wOut)
    );

    dff #(.N(2*B+1)) D2(
      .clk(clk),
      .rst_n(rst_n),
      .en(en),
      .d(sum),
      .q(wSum)
    );

endmodule
""")
    file.close()